#
#   Script creación base de datos proyecto final IAW Verum Populi
#

drop database if exists proyecto_iaw;
create database if not exists proyecto_iaw;
use proyecto_iaw;

create table asistencia(
    id int(11) auto_increment not null,
	id_alumno int(11) not null,
	#id_profesor int(11) not null,
	fecha varchar(40) not null,
    estado varchar(30) not null, # Asistencia/Falta/FaltaJustificada
	primary key (id),
	UNIQUE KEY NoAsistDuplicadas(id_alumno,fecha)
);

create table alumno(
    id int(11) auto_increment primary key,
	nombre varchar(25),
	apellidos varchar(60),
	localidad varchar(30),
	email varchar(40),
	fecha_nac date,
	id_profesor int(11),
    id_clase int(11)
);

create table clase(
	id int(11) auto_increment primary key,
    nom_clase varchar(20),
    id_profesor int(11)
);

create table profesor(
    id int(11) auto_increment primary key,
	id_user_ionauth int,
	usuario varchar(20) unique,
	#contrasena
	nombre varchar(25),
	apellidos varchar(60)
);

create table login(
    id int(22) auto_increment primary key,
	hora datetime,
	id_profesor int(11)
);

alter table asistencia add foreign key(id_alumno) references alumno(id);
#alter table asistencia add foreign key(id_profesor) references profesor(id);
alter table alumno add foreign key(id_profesor) references profesor(id);
alter table alumno add foreign key(id_clase) references clase(id);
create index INDEX_id_user_ionauth on profesor(id_user_ionauth);
alter table clase add foreign key(id_profesor) references profesor(id_user_ionauth);
alter table login add foreign key(id_profesor) references profesor(id);



#insert into profesor (usuario,nombre,apellidos) values
#('profe1','paco','ElMejor'),
#('profe2','jose','ElCampeon'),
#('profe3','carlos','ElCrack');

insert into clase (nom_clase,id_profesor) values 
('1ESO','5'),
('2ESO','5'),
('3ESO','5'),
('4ESO','6'),
('1BACH','6'),
('2BACH','7');

insert into alumno (nombre,apellidos,localidad,email,fecha_nac,id_profesor,id_clase) values
('Sebas','Lopez Mero','Valencia','slopez@gmail.com','2001-03-08','5','13'),
('Maria','Lopez Lima','Paterna','mlopez@gmail.com','2002-11-18','5','13'),
('Lucia','Alcantara Segovia','Valencia','lalcantara@gmail.com','2001-04-22','5','13'),
('Cristina','Perez Galdos','Valencia','cperez@gmail.com','2001-02-28','5','13'),
('Jorge','Melano Sabio','Valencia','jmelano@gmail.com','2002-01-31','5','13'),
('Samuel','De Luque','Alboraya','sdeluque@gmail.com','2002-10-02','5','13'),
('Lucas','Manzano Ramirez','Mislata','lmanzano@gmail.com','2002-11-08','5','13');

INSERT INTO alumno (nombre, apellidos, localidad, email, fecha_nac, id_profesor, id_clase)
VALUES
('Laura', 'García Fernández', 'Valencia', 'lgarciafernandez@correo.es', '2003-02-12', 5, 14),
('Javier', 'González Sánchez', 'Valencia', 'jgonzalezsanchez@correo.es', '2002-05-24', 5, 14),
('Sofía', 'Rodríguez Martínez', 'Valencia', 'srodriguezmartinez@correo.es', '2004-11-08', 5, 14),
('Adrián', 'Sánchez López', 'Valencia', 'asanchezlopez@correo.es', '2003-09-19', 5, 14),
('Lucía', 'Martínez Gómez', 'Valencia', 'lmartinezgomez@correo.es', '2002-03-15', 5, 14),
('Daniel', 'González García', 'Paterna', 'dgonzalezgarcia@correo.es', '2004-07-31', 5, 14),
('Carmen', 'López Martínez', 'Valencia', 'clopezmartinez@correo.es', '2003-01-23', 5, 14),
('Mario', 'Pérez Sánchez', 'Valencia', 'mperezsanchez@correo.es', '2002-06-05', 5, 14),
('Marina', 'Sánchez Martínez', 'Manises', 'msanchezmartinez@correo.es', '2004-12-16', 5, 14),
('Hugo', 'García Sánchez', 'Valencia', 'hgarciasanchez@correo.es', '2003-10-11', 5, 14),
('Alicia', 'Fernández García', 'Valencia', 'afernandezgarcia@correo.es', '2002-04-14', 5, 14),
('Iván', 'Martínez Sánchez', 'Valencia', 'imartinezsanchez@correo.es', '2004-08-22', 5, 15),
('Valeria', 'Sánchez Pérez', 'Valencia', 'vsanchezperez@correo.es', '2003-02-01', 5, 15),
('David', 'García Martínez', 'Sedavi', 'dgarciamartinez@correo.es', '2002-05-13', 5, 15),
('Cristina', 'Pérez Fernández', 'Valencia', 'cperezfernandez@correo.es', '2004-10-27', 5, 15),
('Santiago', 'López Sánchez', 'Valencia', 'slopezsanchez@correo.es', '2003-08-09', 5, 15),
('Ana', 'Martínez Fernández', 'Valencia', 'amartinezfernandez@correo.es', '2002-02-18', 5, 15),
('María', 'González Pérez', 'Torrente', 'mgonzalezperez@correo.es', '2003-03-26', 6, 16),
('Juan', 'Pérez Martínez', 'Valencia', 'jperezmartinez@correo.es', '2002-08-04', 6, 16),
('Natalia', 'Fernández Sánchez', 'Valencia', 'nfernandezsanchez@correo.es', '2004-12-12', 6, 16),
('Luis', 'Martínez Pérez', 'Torrente', 'lmartinezperez@correo.es', '2003-07-14', 6, 16),
('Lucas', 'García Rodríguez', 'Valencia', 'lgarciarodriguez@correo.es', '2002-01-15', 6, 16),
('Carla', 'López Fernández', 'Valencia', 'clopezfernandez@correo.es', '2004-05-07', 6, 16),
('Jorge', 'González Martínez', 'Valencia', 'jgonzalezmartinez@correo.es', '2003-11-23', 6, 16),
('Sara', 'Sánchez Fernández', 'Valencia', 'ssanchezfernandez@correo.es', '2002-04-17', 6, 16),
('Miguel', 'Martínez López', 'Valencia', 'mmartinezlopez@correo.es', '2004-09-09', 6, 16),
('Raquel', 'Pérez Sánchez', 'Valencia', 'rperezsanchez@correo.es', '2003-01-02', 6, 17),
('Andrés', 'García Pérez', 'Algemesi', 'agarciaperez@correo.es', '2002-06-24', 6, 17),
('Claudia', 'López Rodríguez', 'Valencia', 'clopezrodriguez@correo.es', '2004-11-11', 6, 17),
('Fernando', 'González Fernández', 'Valencia', 'fgonzalezfernandez@correo.es', '2003-09-12', 6, 17),
('Marta', 'Sánchez Martínez', 'Valencia', 'msanchezmartinez@correo.es', '2002-03-29', 6, 17),
('Alonso', 'Martínez Sánchez', 'Valencia', 'amartinezsanchez@correo.es', '2004-07-01', 6, 17),
('Paula', 'García López', 'Valencia', 'pgarcialopez@correo.es', '2003-02-04', 6, 17),
('Francisco', 'Fernández Rodríguez', 'Valencia', 'ffernandezrodriguez@correo.es', '2002-05-16', 6, 17),
('Aitana', 'López Sánchez', 'Alicante', 'alopezsanchez@correo.es', '2004-10-19', 6, 17),
('David', 'García Pérez', 'Castellar', 'dgarcia@correo.es', '2003-03-26', 7, 18),
('Cristina', 'Fernández Sánchez', 'Valencia', 'cfernandez@correo.es', '2002-08-04', 7, 18),
('Pedro', 'Martínez Pérez', 'Valencia', 'pmartinez@correo.es', '2004-12-12', 7, 18),
('Manuel', 'González Rodríguez', 'Valencia', 'mgonzalez@correo.es', '2003-07-14', 7, 18),
('Laura', 'López Fernández', 'Valencia', 'llopez@correo.es', '2002-01-15', 7, 18),
('José', 'García Rodríguez', 'Valencia', 'jgarcia@correo.es', '2004-05-07', 7, 18),
('Sergio', 'Pérez Martínez', 'Valencia', 'sperez@correo.es', '2003-11-23', 7, 18),
('Lucía', 'Sánchez Fernández', 'Valencia', 'lsanchez@correo.es', '2002-04-17', 7, 18),
('Rocío', 'Martínez López', 'Valencia', 'rmartinez@correo.es', '2004-09-09', 7, 18),
('Javier', 'Pérez Sánchez', 'Valencia', 'jperez@correo.es', '2003-01-02', 7, 18),
('Marina', 'González Martínez', 'Valencia', 'mgonzalez2@correo.es', '2002-06-24', 7, 18),
('Rubén', 'López Rodríguez', 'Valencia', 'rlopez@correo.es', '2004-11-11', 7, 18),
('Carmen', 'García Fernández', 'Valencia', 'cgarcia@correo.es', '2003-09-12', 7, 18),
('Andrea', 'Sánchez Martínez', 'Valencia', 'asanchez@correo.es', '2002-03-29', 7, 18),
('Adrián', 'Martínez Sánchez', 'Valencia', 'amartinez2@correo.es', '2004-07-01', 7, 18);


delimiter //
create trigger usersXporfesor_insert after insert on users
for each row
begin
	insert into profesor (usuario,nombre,apellidos,id_user_ionauth) values
	(new.username,new.first_name,new.last_name,new.id);
end
//

delimiter //
create trigger BorrarAsistenciasUsuarios before delete on alumno
for each row
begin
	delete from asistencia where id_alumno=old.id;
end
//