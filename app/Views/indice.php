
<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>
    Panel Admin - Inicio
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>

  <!-- /.navbar -->


  <!-- Content Wrapper. Contains page content -->

   
 
    <!-- /.content-header -->

    <!-- CARTASSSSSSSSSS-->


    <!----------------------------------------------------------------------------------------->
   

        <!-- Small boxes (Stat box) -->
        <div class="row" style="margin: auto; margin-top: 30px">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box " style="background: linear-gradient(45deg,#E1E1E1,#E8E8E8);">
              <div class="inner">
                <h3><?= $alumnos[0]->a?></h3>

                <p>Alumnos Inscritos</p>
              </div>
              <div class="icon">
                <i class="ion ion-man" style="color: black"></i>
              </div>
              <a href="alumnos/" class="small-box-footer bg-dark">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box " style="background: linear-gradient(45deg,#E1E1E1,#E8E8E8);">
              <div class="inner">
                <h3> <?= $clases[0]->a?><sup style="font-size: 20px"></sup></h3>

                <p>Clases</p>
              </div>
              <div class="icon">
                <i class="ion ion-home" style="color: black;"></i>
              </div>
              <a href="clases/" class="small-box-footer bg-dark">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box"  style="background: linear-gradient(45deg,#E1E1E1,#E8E8E8);">
              <div class="inner">
                <h3><?= $profes[0]->a?></h3>

                <p>Profesores</p>
              </div>
              <div class="icon">
                <i class="ion ion-briefcase" style="color: black;"></i>
              </div>
              <a href="profesores/" class="small-box-footer bg-dark">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        
       
            <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box " style="background: linear-gradient(45deg,#E1E1E1,#E8E8E8);">
              <div class="inner">
                <h3><?= $users[0]->a?></h3>

                <p>Usuarios</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add" style="color: black"></i>
              </div>
              <a href="auth/" class="small-box-footer bg-dark">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
       </div>
  
<?= $this->endSection('BODY') ?>