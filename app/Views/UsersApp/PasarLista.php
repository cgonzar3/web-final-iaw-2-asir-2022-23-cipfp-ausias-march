<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML1') ?>

<?= $this->section('HEAD') ?>
Alumnos <?= $NombreClase['0']->nom_clase ?>
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
<!--<?= print_r($ExisteAsistenciaHoy)?>-->
<div class="FondoIMG" style="width: auto; height: auto">

<html>
    
    <div class="m-auto w-75 pl-4 pt-5">
        <h1 class="text-dark">Asistencias (<?= date('l jS F Y'); ?>)</h1>
    </div>
    
    <div class="container-fluid w-75 h-100">
        
                             
        <?= form_open('username/pasar_lista/'.$CLASE)?>
                
        
        <table class="table table-borderless CARTA p-4" id="myTable" style="width: auto; margin: auto; border-radius: 20px">
            <thead> <br>
                <tr >                  
                    <th>
                        Nombre
                    </th>
                    <th>
                        Apellidos
                    <th>
                        <i>Acciones</i>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($SelectAlumnos as $al): ?>
                <tr class="ExisteAsistencia">
                    <td style="width: 200px">
                            <?= $al->nombre ?>
                    </td>
                    <td style="width: 300px">
                            <?= $al->apellidos ?>
                        </td>
                         
                        <td>
                        
                                <!--Este forach comprueba si existe una entrada en la tabla asistencia
                                    con la id_alumno y fecha de cada alumno-->
                                <?php foreach ($ExisteAsistenciaHoy as $al2): ?>
                                
                                    <?php  if ( $al->id == $al2->id_alumno) : ?>
                                <span style="opacity: 30%"><i>Enviado</i></span>
                                    <div style="opacity: 0%; pointer-events: none; position: absolute; margin-top: -2000px">
                                        <?php endif ?>
                            
                                <?php endforeach; ?>

                                                       
                            <div class="div_radio1"><?= form_radio('name-'.$al->id,$al->id.'-asiste',$checked = false,['class' => 'RadioVP', 'title' => 'Asiste']) ?></div>
                            <div class="div_radio2"><?= form_radio('name-'.$al->id,$al->id.'-falta',$checked = false,['class' => 'RadioVP', 'title' => 'Falta']) ?></div>
                            <div class="div_radio3"><?= form_radio('name-'.$al->id,$al->id.'-falta_j',$checked = false,['class' => 'RadioVP', 'title' => 'Falta (justificada)'])  ?></div>
                            
                                <!--Este forach comprueba si existe una entrada en la tabla asistencia
                                    con la id_alumno y fecha de cada alumno-->
                                <?php foreach ($ExisteAsistenciaHoy as $al2): ?>
                                
                                    <?php  if ( $al->id == $al2->id_alumno) : ?>
                                    </div>
                                    <?php endif ?>
                            
                                <?php endforeach; ?>                                        
                                        
                                        
                        </td>

                   

                    </tr>
                <?php endforeach; ?>
                
            </tbody>
        </table>
        <!--FIN TABLA-->
        <?= form_submit('boton_submit','Guardar',['class'=>'btn btn-primary rounded-lg mt-3']) ?>
        <a href="<?= base_url('/username/clase/'.$CLASE) ?>">
            <button type="button" class="btn btn-outline-dark rounded-lg mt-3">Volver atrás</button>
        </a>
        <?= form_close() ?>

    </div>
    <script type="text/javascript">
        function alerta()
    {
        var respuesta = confirm("¿Deseas eliminar al Alumno?") ;
        if (respuesta == true) {
            mensaje = "Alumno Eliminado";
        } else {
            mensaje = "Operación Cancelada";
        }
        document.getElementById("ejemplo").innerHTML = mensaje;
    }
</script>
</div>
    

    <?= $this->endSection('BODY') ?>