<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML1') ?>

<?= $this->section('HEAD') ?>
Asistencias (<?= $NombreAlumno['0']->nombre ?> <?= $NombreAlumno['0']->apellidos ?>)
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
<!---->
<div class="FondoIMG" style="width: 100%;">

<html>
    
    <div class="m-auto w-75 pt-5">
        
    </div>
    
    <div class="container-fluid h-100 mt-3" style="width: 95%">
        
        <h1 class="text-dark">Todas las asistencias de (<?= $NombreAlumno['0']->nombre ?>) </h1>
        <br>
        <table class="table table-borderless CARTA p-4" id="myTable" style="margin: auto; border-radius: 20px">
            <thead> 
                <tr>                  
                    <th>
                        Nombre
                    </th>
                    <th>
                        Apellidos
                    </th>  
                    <th>
                        Fecha
                    </th>   
                    <th>
                        Estado
                    </th>                      

                </tr>
            </thead>
            <tbody>
                <?php foreach ($SelectAsistencias as $asistencia): ?>
                <tr>
                    
                    <td style="width: 100px">
                        <?= $NombreAlumno['0']->nombre ?>
                    </td>
                    
                    <td style="width: 300px">
                        <?= $NombreAlumno['0']->apellidos ?>
                    </td>

                    <td style="width: 300px">
                        <?= $asistencia->fecha ?>  
                    </td>   
                    <td style="width: 400px">
                        <p <?php if ( $asistencia->estado == "falta" ): ?> style="position:relative; top: -12px" <?php endif ?>>
                        
                        <?= $asistencia->estado ?>  
                        
                        <?php if ( $asistencia->estado == "falta" ): ?>
                        
                        <a href="<?= base_url('/username/asistencias-de/justif/'.$NombreAlumno['0']->id."/".$asistencia->id) ?>">
                        <button type="button" class="btn btn-sm btn-outline-success rounded-lg mt-3 position-relative" style="top:-10px; left: 8px">
                           justificar</button>
                        </p>    
                    </a>
                        
                        
                        
                        <?php endif ?>
                        
                        
                    </td>                     
                    
                    
                </tr>
                <?php endforeach; ?>
                
            </tbody>
        </table>
        <!--FIN TABLA-->

        <a href="javascript:history.back()">
            <button type="button" class="btn btn-outline-dark rounded-lg mt-3">Volver atrás</button>
        </a>


    </div>
    <script type="text/javascript">
        function alerta()
    {
        var respuesta = confirm("¿Deseas eliminar al Alumno?") ;
        if (respuesta == true) {
            mensaje = "Alumno Eliminado";
        } else {
            mensaje = "Operación Cancelada";
        }
        document.getElementById("ejemplo").innerHTML = mensaje;
    }
</script>
</div>
    

    <?= $this->endSection('BODY') ?>