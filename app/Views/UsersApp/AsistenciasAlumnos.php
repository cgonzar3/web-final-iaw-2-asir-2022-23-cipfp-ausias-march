<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML1') ?>

<?= $this->section('HEAD') ?>
<?= $NombreClase['0']->nom_clase ?> - Todas las asistencias
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
<!---->
<div class="FondoIMG" style="height: 100vh;">

<html>
    
    <div class="m-auto w-75 pt-5">
        
    </div>
    
    <div class="container-fluid h-100 mt-3" style="width: 95%">
        
        <h1 class="text-dark">Ver asistencias (<?= $NombreClase['0']->nom_clase ?>) </h1>
        <br>
        <table class="table table-borderless CARTA p-4" id="myTable" style="margin: auto; border-radius: 20px">
            <thead> 
                <tr>                  
                    <th>
                        
                        <i>Acciones</i>
                    </th>
                    <th>
                        Nombre
                    </th>
 
                    <th>
                        Apellidos
                    </th>                        

                </tr>
            </thead>
            <tbody>
                <?php foreach ($SelectAlumnos as $al): ?>
                <tr>
                    
                    <td style="width: 60px">
                    <a href="<?= base_url('/username/asistencias-de/'.$al->id) ?>">
                        <button type="button" class="btn btn-sm btn-outline-dark rounded-lg mt-3 position-relative" style="top: -21px">
                            <i class="bi bi-search"></i>
                            &nbsp Asistencias</button>
                    </a>
                    </td>
                    
                    <td style="width: 200px">
                            <?= $al->nombre ?>
                    </td>

                    <td style="width: 400px">
                        <?= $al->apellidos ?>
                    </td>                    
                    
                </tr>
                <?php endforeach; ?>
                
            </tbody>
        </table>
        <!--FIN TABLA-->

        <a href="<?= base_url('/username/clase/'.$CLASE) ?>">
            <button type="button" class="btn btn-outline-dark rounded-lg mt-3">Volver atrás</button>
        </a>
        <br><br><br>

    </div>
    <script type="text/javascript">
        function alerta()
    {
        var respuesta = confirm("¿Deseas eliminar al Alumno?") ;
        if (respuesta == true) {
            mensaje = "Alumno Eliminado";
        } else {
            mensaje = "Operación Cancelada";
        }
        document.getElementById("ejemplo").innerHTML = mensaje;
    }
</script>
</div>

    <?= $this->endSection('BODY') ?>