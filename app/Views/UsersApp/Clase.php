<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML1') ?>

<?= $this->section('HEAD') ?>
    <?= $NombreClase['0']->nom_clase?>
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
       
    <div class="FondoIMG" style="width: 100%; height: 100%">
        <div class="w-75 p-4 pt-5  text-left m-auto">
        <h1 style="font-size: 50px" class="text-left w-50 d-inline-block"><?=$NombreClase['0']->nom_clase?></h1>
        <div class="text-right  w-50 d-inline-block float-right">
            <a href="<?= base_url('/username/inicio') ?>">
            <button type="button" class="btn btn-outline-dark rounded-lg mt-3 ml-4">Volver atrás</button>
            </a>             
            <a href="<?= base_url('/auth/logout') ?>">
                <button type="button" class="btn btn-outline-danger rounded-lg mt-3">Salir</button>
            </a>
        </div>       
    </div>
        
        

    
    <div class="w-75 m-auto h-50 p-4 CARTA">
        <h2 class="pl-3 pt-4">
            <?= date('l jS F Y'); ?>
        </h2>
        <a href="<?= base_url('username/pasar_lista/'.$CLASE)?>">
            <button type="button" class="btn btn-outline-primary rounded-pill ml-3">
                Control de asistencia
            </button>
        </a>
        
        
    </div>
    
    <div class="row m-auto h-25 w-75 CARTA" >
        <div class="rounded-lg col">
            <h2 class="pl-3 pt-4">Datos Alumnos <?=$NombreClase['0']->nom_clase?></h2>
        <a href="<?= base_url('username/datos-alumnos/'.$CLASE)?>">
            <button type="button" class="btn btn-outline-primary rounded-pill ml-3">
                Ver datos
            </button>
        </a>
        </div>

        <div class=" rounded-lg col">
            <h2 class="pl-3 pt-4">Todas las asistencias</h2>
            <a href="<?= base_url('username/asistencias-alumnos/'.$CLASE)?>" class="pl-3">
                Mostrar todas las asistencias por alumnos
            </a>
        </div>
        
    </div>
        
    
    </div>
<?= $this->endSection('BODY') ?>