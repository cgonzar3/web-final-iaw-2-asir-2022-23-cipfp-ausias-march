<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML1') ?>

<?= $this->section('HEAD') ?>
    Inicio - Bienvenido <?= $usuario ?>
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
      
       
    <div class="FondoIMG" style="width: auto; height: 100%">
    <div class="container w-75 p-4 pt-5  text-left m-auto ">
        <h1 class="text-left w-50 d-inline-block">Bienvenido, <?=$usuario?>  
        </h1>
        <div class="text-right  w-50 d-inline-block float-right">
        <a href="<?= base_url('/') ?>">
            <button type="button" class="btn btn-primary rounded-lg mt-2">
                
                <?php if ($ionAuth->loggedIn()): ?>

                <?php $user = $ionAuth->user()->row(); ?>
                <i class="bi bi-person-fill mr-1"></i>
                <?= $user->first_name . ' ' . $user->last_name ?>   

                <?php else: ?>

                <?php endif; ?>
                
            </button>
        </a>
        <a href="<?= base_url('/auth/logout ') ?>">
            <button type="button" class="btn btn-outline-danger rounded-lg mt-2">Salir</button>
        </a>
        </div>       
    </div>
        

    
    <div class="container w-75  m-auto">
    <h4 class="pl-3 pt-4">Tus clases:</h4>
    
    <div class="container">
        <div class="card-columns w-100" >
            
            <?php foreach ($SelectClases as $clase) : ?>
            <div class="card CARTA pt-1 pb-3">

                <div class="card-body text-center">
                    <h1>
                        <?= $clase->nom_clase ?>
                    </h1>
                    <a href="<?= base_url('/username/clase/'.$clase->id) ?>">
                        <button type="button" class="btn btn-sm btn-outline-primary rounded-pill" style="font-size: 15px">
                            Acceder
                        </button>
                    </a>
                </div>
            </div>
            <?php endforeach; ?>

        </div>
    </div>
    </div>
    </div>
    
<?= $this->endSection('BODY') ?>