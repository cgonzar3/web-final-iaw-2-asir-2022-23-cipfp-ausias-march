<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>
    Editar datos Alumno
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
     <div class="container-fluid mt-5 w-75" style="border: solid 2px">
         <br>
         <center><h1 class="text-dark">Modificar Datos de Alumnos</h1></center>
         <br>
   
        <form action="<?= Base_url('alumnos/editar/'.$al->id)?>" method="post" class="w-75 m-auto" >
        <div class="form-group">
            <?= form_label('Nombre:', 'nombre', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('nombre',set_value('nombre',$al->nombre),['class'=>'form_control col-9', 'id'=>'nombre']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Apellidos:', 'apellidos', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('apellidos',set_value('apellidos',$al->apellidos),['class'=>'form_control col-9', 'id'=>'apellidos']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Localidad:', 'localidad', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('localidad',set_value('localidad',$al->localidad),['class'=>'form_control col-9', 'id'=>'localidad']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Email:', 'email', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('email',set_value('email',$al->email),['class'=>'form_control col-9', 'id'=>'email']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Nacimiento:', 'fecha_nac', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('fecha_nac',set_value('fecha_nac',$al->fecha_nac),['class'=>'form_control col-9', 'id'=>'fecha_nac']) ?>
        </div>
             <label for="id_clase" class="col-sm-2 col-form-label">Clase:</label>
                    <select name="id_clase" id="id_clase"class="form_control col-9">
                         <?php foreach ($TodasLasClases as $clas): ?>
                        <option value=" <?= $clas->id ?>"> <?= $clas->nom_clase ?></option>
          <?php endforeach; ?>
                    </select><br><br>
                    <center> <input type="submit" name="boton" value="Enviar" /><br><br></center>
    </form>
       </div>

<?= $this->endSection('BODY') ?>