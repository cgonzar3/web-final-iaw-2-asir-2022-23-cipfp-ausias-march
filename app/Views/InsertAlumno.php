<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>
Insertar nuevo Alumno
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>

<div class="container-fluid mt-5 w-75" style="border: solid 2px">
    <center><h1 class="text-dark">Nuevos Alumno</h1></center>

    <?= form_open('nuevoalumno') ?>
    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?= $field ?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <!--Para rellenar el formulario con los datos del alumno a editar
        pongo $ALUMNO['0'] en lugar de poner solo $ALUMNO porque al
        hacer el "Select * where ID..." el resultado se almacena
        como array dentro del array. Por lo tanto al referenciarlo
        aquí hay que decir: dentro de $ALUMNO, en el Array "0"-> nombre
    
        Si no se entiende hacer un print_r de $array en el controlador
        para verlo más claramente.-->

    <?= form_label('Nombre: ', 'nombre', ['class' => 'col-sm-2 col-form-label']) ?>
    <?= form_input('nombre', set_value('nombre', ''), ['class' => 'form_control col-9', 'id' => 'nombre']) ?>
    <br>
    <?= form_label('Apellidos: ', 'apellidos', ['class' => 'col-sm-2 col-form-label']) ?>
    <?= form_input('apellidos', set_value('apellidos', ''), ['class' => 'form_control col-9', 'id' => 'apellidos']) ?>
    <br>
    <?= form_label('Localidad:', 'localidad', ['class' => 'col-sm-2 col-form-label']) ?>
    <?= form_input('localidad', set_value('localidad', ''), ['class' => 'form_control col-9', 'id' => 'localidad']) ?>
    <br>
    <?= form_label('Correo electrónico: ', 'email', ['class' => 'col-sm-2 col-form-label']) ?>
    <?= form_input('email', set_value('email', 'tucorreo@gmail.com'), ['class' => 'form_control col-9', 'id' => 'email']) ?>
    <br>
    <?= form_label('Fecha de Nacimiento: ', 'fecha_nac', ['class' => 'col-sm-2 col-form-label']) ?>
    <?= form_input('fecha_nac', set_value('fecha_nac', 'YYYY-MM-DD'), ['class' => 'form_control col-9', 'id' => 'fecha_nac']) ?>
    <br>
    <label for="id_clase" class="col-sm-2 col-form-label">Clase:</label>
    
    <select name="id_clase" id="id_clase"  class="form_control col-9">
    <?php foreach ($TodasLasClases as $clas): ?>
            <option value=" <?= $clas->id ?>"> <?= $clas->nom_clase ?></option>
    <?php endforeach; ?>
      </select>


    <?= form_submit('boton_submit', 'Guardar', ['class' => 'btn btn-dark m-3']) ?>

    <?= form_close() ?>

</div>




<?= $this->endSection('BODY') ?>