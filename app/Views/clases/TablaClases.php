<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>

Gestión de Clases
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
<html>


    <div class="container-fluid w-50">
        <br>  <center><h1 class="text-dark" style="margin: auto;">Clases</h1></center>
        <div style="float:right;"><a style="max-height: 36px;" class="btn btn-dark" href="<?= site_url('nuevaclase') ?>"> Insertar</a></div>                        
        <table class="table table-striped" id="myTable">
            <thead>
                <tr>                  
                    <th>
                       Clases
                    </th>
                     <th>
                       Profesor
                    </th>
                    <th>
                        <i>Acciones</i>
                    </th>

                </tr>
            </thead>
            <tbody>
                <?php foreach ($TodasLasClases as $clas): ?>
                    <tr>
                        <td>
                            <?= $clas->nom_clase ?>
                            </td> 
                            <td>
                            <?= $clas->nombre ?> <?= $clas->apellidos ?>
                            </td> 
                            <td >

                            <a  href="<?= base_url('/clases/editar/' . $clas->id) ?>" title="Editar datos de <?= $clas->nom_clase ?>">
                                <span class="bi bi-pen-fill text-dark"></span>
                            </a>                        
                            <a   href="<?= base_url('/clases/borrar/' . $clas->id) ?>" title="Borrar la clase <?= $clas->nom_clase ?>"
                               class="btn btn-sm btn-ligth" onclick="return confirm('¿Estás seguro de borrarlo?')" >
                                <span class="bi bi-trash-fill text-dark"></span>
                            </a>
                           
                        </td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
    
    <!--FIN TABLA-->

    <?= $this->endSection('BODY') ?>