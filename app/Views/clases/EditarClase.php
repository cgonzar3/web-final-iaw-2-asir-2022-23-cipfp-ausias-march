<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>
Editar datos Clases
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
     <div class="container-fluid mt-5 w-75" style="border: solid 2px">
         <br>
         <center><h1 class="text-dark">Modificar Datos de Clases</h1></center>
         <br>
   
        <form action="<?= Base_url('clases/editar/'.$clas->id)?>" method="post" class="w-75 m-auto" >
            
            <?= form_label('Nombre Clase:', 'nom_clase', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('nom_clase',set_value('nom_clase',$clas->nom_clase),['class'=>'form_control col-9', 'id'=>'nom_clase']) ?>
      
             <label for="id_clase" class="col-sm-2 col-form-label">Profesor:</label>
                    <select name="id_profesor" id="id_profesor"class="form_control col-9">
                         <?php foreach ($TodosLosProfesores as $prof): ?>
                        <option value=" <?= $prof->id ?>"> <?= $prof->nombre ?> <?= $prof->apellidos?></option>
          <?php endforeach; ?>
      </select>
             <br><BR>
             <center><input type="submit" name="boton" value="Enviar" /><br><br></center>
    </form>
       </div>

<?= $this->endSection('BODY') ?>