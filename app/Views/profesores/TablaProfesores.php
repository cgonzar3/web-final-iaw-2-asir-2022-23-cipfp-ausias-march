<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>

Gestión de Profesores
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
<html>


    <div class="container-fluid w-50 h-100">
        <br>  <center><h1 class="text-dark" style="margin: auto;">Profesores</h1></center>
        <div style="float:right;"><a style="max-height: 36px;" class="btn btn-dark" href="<?= site_url('nuevoprofesor') ?>"> Insertar</a></div>                        
        <table class="table table-striped" id="myTable">
            <thead>
                <tr>                  
                    <th>
                       Usuario
                    </th>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Apellidos
                    </th>
                    <th>
                        <i>Acciones</i>
                    </th>

                </tr>
            </thead>
            <tbody>
                <?php foreach ($TodosLosProfesores as $prof): ?>
                    <tr>
                        <td>
                            <?= $prof->usuario ?>
                        </td>
                        <td>
                            <?= $prof->nombre ?>
                        </td>
                        <td>
                            <?= $prof->apellidos ?>
                        </td>    
                        <td>

                            <a href="<?= base_url('/profesores/editar/' . $prof->id) ?>" title="Editar datos de <?= $prof->nombre ?>">
                                <span class="bi bi-pen-fill text-dark"></span>
                            </a>                        
                        <a href="<?= base_url('/profesores/borrar/' . $prof->id) ?>" title="Borrar a <?= $prof->nombre ?> 
                           class="btn btn-sm btn-ligth" onclick="return confirm('¿Estás seguro de borrarlo?')" >
                                <span class="bi bi-trash-fill text-dark"></span>
                            </a>
                           
                        </td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
 

    <!--FIN TABLA-->

    <?= $this->endSection('BODY') ?>