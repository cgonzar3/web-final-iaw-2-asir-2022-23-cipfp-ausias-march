<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>
Insertar nuevo Profesor
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>

<div class="container-fluid mt-5 w-75" style="border: solid 2px">
    <center><h1 class="text-dark">Nuevos Profesores</h1></center>

    <?= form_open('nuevoprofesor') ?>
    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?= $field ?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <!--Para rellenar el formulario con los datos del alumno a editar
        pongo $ALUMNO['0'] en lugar de poner solo $ALUMNO porque al
        hacer el "Select * where ID..." el resultado se almacena
        como array dentro del array. Por lo tanto al referenciarlo
        aquí hay que decir: dentro de $ALUMNO, en el Array "0"-> nombre
    
        Si no se entiende hacer un print_r de $array en el controlador
        para verlo más claramente.-->
    <?= form_label('Usuario:', 'usuario', ['class' => 'col-sm-2 col-form-label']) ?>
    <?= form_input('usuario', set_value('usuario', ''), ['class' => 'form_control col-9', 'id' => 'usuario']) ?>
    <br>
    <?= form_label('Nombre: ', 'nombre', ['class' => 'col-sm-2 col-form-label']) ?>
    <?= form_input('nombre', set_value('nombre', ''), ['class' => 'form_control col-9', 'id' => 'nombre']) ?>
    <br>
    <?= form_label('Apellidos: ', 'apellidos', ['class' => 'col-sm-2 col-form-label']) ?>
    <?= form_input('apellidos', set_value('apellidos', ''), ['class' => 'form_control col-9', 'id' => 'apellidos']) ?>
    <br>
    <?= form_submit('boton_submit', 'Guardar', ['class' => 'btn btn-dark m-3']) ?>

    <?= form_close() ?>

</div>




<?= $this->endSection('BODY') ?>