<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>
    Editar datos Profesor
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
     <div class="container-fluid mt-5 w-75" style="border: solid 2px">
         <br>
         <center><h1 class="text-dark">Modificar Datos de Profesores</h1></center>
         <br>
   
        <form action="<?= Base_url('profesores/editar/'.$prof->id)?>" method="post" class="w-75 m-auto" >
             <div class="form-group">
            <?= form_label('Usuario:', 'usuario', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('usuario',set_value('localidad',$prof->usuario),['class'=>'form_control col-9', 'id'=>'usuario']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Nombre:', 'nombre', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('nombre',set_value('nombre',$prof->nombre),['class'=>'form_control col-9', 'id'=>'nombre']) ?>
        </div>
        <div class="form-group">
            <?= form_label('Apellidos:', 'apellidos', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('apellidos',set_value('apellidos',$prof->apellidos),['class'=>'form_control col-9', 'id'=>'apellidos']) ?>
        </div>
            <input type="submit" name="boton" value="Enviar" /><br><br>
    </form>
       </div>

<?= $this->endSection('BODY') ?>