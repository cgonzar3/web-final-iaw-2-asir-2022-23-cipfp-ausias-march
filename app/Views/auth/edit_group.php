<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>

Auth Admin

<?= $this->endSection('HEAD') ?>
<?= $this->section('BODY') ?>
<br>
<div style="margin: auto; margin-top: 8%;">
<center><h1>Editar Grupos</h1></center>


<center><div id="infoMessage"><?php echo $message;?></div>
    <div>
<?php echo form_open(current_url());?>
        <br><br>
      <p>
            <?php echo form_label(lang('Auth.edit_group_name_label'), 'group_name');?> 
            <?php echo form_input($group_name);?>
      </p>

      <p>
            <?php echo form_label(lang('Auth.edit_group_desc_label'), 'description');?> 
            <?php echo form_input($group_description);?>
      </p>

      <p><?php echo form_submit('submit', lang('Auth.edit_group_submit_btn'));?></p>

<?php echo form_close();?>
      </div></div>
  <?= $this->endSection('BODY') ?>