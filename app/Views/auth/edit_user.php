<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>

Auth Admin

<?= $this->endSection('HEAD') ?>
<?= $this->section('BODY') ?>

    <br>
<center><h1 style="margin-left: 50px">  Editar Usuario</h1></center>
<br>



<?php echo form_open(uri_string());?>

    <center>  <p>
            <?php echo form_label('Nombre:', 'first_name', ['class' => 'col-sm-2 col-form-label']);?> 
            <?php echo form_input($first_name);?>
      </p></center>

   <center>   <p>
            <?php echo form_label('Apellido:', 'last_name', ['class' => 'col-sm-2 col-form-label']);?> 
            <?php echo form_input($last_name);?>
      </p></center>

    <center>  <p>
            <?php echo form_label('Dominio:', 'company', ['class' => 'col-sm-2 col-form-label']);?>
            <?php echo form_input($company);?>
      </p></center>

    <center>  <p>
            <?php echo form_label('Teléfono:', 'phone', ['class' => 'col-sm-2 col-form-label']);?> 
            <?php echo form_input($phone);?>
      </p></center>
<center>
      <p>
            <?php echo form_label('Cambiar Contraseña:', 'password', ['class' => 'col-sm-2 col-form-label']);?> 
            <?php echo form_input($password);?>
      </p></center>

     <center> <p>
            <?php echo form_label('Confirmar Contraseña:', 'password_confirm' , ['class' => 'col-sm-2 col-form-label']);?>
            <?php echo form_input($password_confirm);?>
      </p></center>

      <?php if ($ionAuth->isAdmin()): ?>

      <center> <h3>Grupos del Usuario</h3></center>
      <center> <div style="margin: auto">
          <?php foreach ($groups as $group):?>
              <label class="checkbox" ">
              <?php
                  $gID = $group['id'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->id) {
                          $checked = ' checked="checked"';
                      break;
                      }
                  }
              ?>
                  
                   <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
              <?php echo htmlspecialchars($group['name'], ENT_QUOTES, 'UTF-8');?>
              </label>
          <?php endforeach?>
          </div></center>
      <?php endif ?>

      <?php echo form_hidden('id', $user->id);?>

      <center>  <p><?php echo form_submit('submit', lang('Auth.edit_user_submit_btn'));?></p></center>

<?php echo form_close();?>
  <?= $this->endSection('BODY') ?>