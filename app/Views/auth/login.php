<?= $this->extend('PLANTILLAS/PlantillaHTML1') ?>

<?= $this->section('HEAD') ?>
    Inicio de sesión
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
    <div style="height: 8em"></div>
<div class="pt-5 m-auto CARTA px-5 py-4" style="border:solid; width: 40%; border-radius: 25px !important;">
    <h1><?php echo lang('Auth.login_heading');?></h1>
    <p class=""><?php echo lang('Auth.login_subheading');?></p>

    

    <div>
    <?php echo form_open('auth/login');?>
    <br>
      <p>
        <?php echo form_label(lang('Auth.login_identity_label'), 'identity');?>
          <br>
        <?php echo form_input($identity,"",['placeholder' => 'usuario@gmail.com']);?>
      </p>

      <p>
        <?php echo form_label(lang('Auth.login_password_label'), 'password');?>
          <br>
        <?php echo form_input($password,"",['placeholder' => 'Contraseña']);?>
      </p>
      <p style="margin-top: -10px; color: #727272; font-weight: 0">
        <?php echo form_label(lang('Auth.login_remember_label'), 'remember');?>
        <?php echo form_checkbox('remember', '1', false, 'id="remember"');?>
      </p>
      <!--<p style="margin-top:20px; font-size: 14px; margin-bottom: -5px"><a href="forgot_password"><?php echo lang('Auth.login_forgot_password');?></a></p>-->
      
      <p style="margin-top: -15px"><?php echo form_submit('submit', lang('Auth.login_submit_btn'),['class'=>'btn btn-lg btn-outline-primary rounded-pill mt-3']);?></p>
      
      <?php echo form_close();?>
      
    </div>
      

    
</div>
    
<?= $this->endSection('BODY') ?>