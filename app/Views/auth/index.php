<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>

Auth Admin

<?= $this->endSection('HEAD') ?>
<?= $this->section('BODY') ?>
<br><br><br>
<center><h1><?php echo lang('Auth.index_heading');?></h1></center>


<br>
<center><table cellpadding=5 cellspacing=10 style="border: 1px solid black"></center>
	<tr style="border: 1px solid black">
		<th style="border: 1px solid black"><?php echo lang('Auth.index_fname_th');?></th>
		<th style="border: 1px solid black"><?php echo lang('Auth.index_lname_th');?></th>
		<th style="border: 1px solid black"><?php echo lang('Auth.index_email_th');?></th>
		<th style="border: 1px solid black"><?php echo lang('Auth.index_groups_th');?></th>
		<th style="border: 1px solid black"><?php echo lang('Auth.index_status_th');?></th>
		<th style="border: 1px solid black"><?php echo lang('Auth.index_action_th');?></th>
	</tr>
	<?php foreach ($users as $user):?>
		<tr style="border: 1px solid black">
            <td style="border: 1px solid black"><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
            <td style="border: 1px solid black"><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
            <td style="border: 1px solid black"><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
			<td>
				<?php foreach ($user->groups as $group):?>
					<?php echo anchor('auth/edit_group/' . $group->id, htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8')); ?><br />
                <?php endforeach?>
			</td>
			<td style="border: 1px solid black"><?php echo ($user->active) ? anchor('auth/deactivate/' . $user->id, lang('Auth.index_active_link')) : anchor("auth/activate/". $user->id, lang('Auth.index_inactive_link'));?></td>
			<td style="border: 1px solid black"><?php echo anchor('auth/edit_user/' . $user->id, lang('Auth.index_edit_link')) ;?></td>
		</tr>
	<?php endforeach;?>
</table>
    <br>
<p><?php echo anchor('auth/create_user', lang('Auth.index_create_user_link'))?> | <?php echo anchor('auth/create_group', lang('Auth.index_create_group_link'))?></p>
   <?= $this->endSection('BODY') ?>