<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>

Auth Admin

<?= $this->endSection('HEAD') ?>
<?= $this->section('BODY') ?>

<br>
<center><h1 >  Crear Usuario</h1></center>
<br>
<div id="infoMessage"><?php echo $message;?></div>

<?php echo form_open('auth/create_user');?>
<center>
      <p>
		<?php echo form_label('Nombre:', 'first_name', ['class' => 'col-sm-2 col-form-label']);?> 
            <?php echo form_input($first_name);?>
      </p></center>
<center>
      <p>
            <?php echo form_label('Apellido:', 'last_name', ['class' => 'col-sm-2 col-form-label']);?> 
            <?php echo form_input($last_name);?>
      </p></center>

      <?php
      if ($identity_column !== 'email') {
          echo '<p>';
          echo form_label(lang('Auth.create_user_identity_label'), 'identity');
          echo '<br />';
          echo \Config\Services::validation()->getError('identity');
          echo form_input($identity);
          echo '</p>';
      }
      ?>
<center>
      <p>
            <?php echo form_label('Dominio:', 'company', ['class' => 'col-sm-2 col-form-label']);?> 
            <?php echo form_input($company);?>
      </p></center>
<center>
      <p>
            <?php echo form_label(lang('Auth.create_user_email_label'), 'email', ['class' => 'col-sm-2 col-form-label']);?> 
            <?php echo form_input($email);?>
      </p></center>
<center>
      <p>
            <?php echo form_label('Teléfono:', 'phone', ['class' => 'col-sm-2 col-form-label']);?> 
            <?php echo form_input($phone);?>
      </p></center>
<center>
      <p>
            <?php echo form_label('Contraseña:', 'password', ['class' => 'col-sm-2 col-form-label']);?> 
            <?php echo form_input($password);?>
      </p></center>
<center>
      <p>
            <?php echo form_label('Confirmar Contraseña:', 'password_confirm', ['class' => 'col-sm-2 col-form-label']);?> 
            <?php echo form_input($password_confirm);?>
      </p></center>


      <center> <p><?php echo form_submit('submit', lang('Auth.create_user_submit_btn', ['class' => 'col-sm-2 col-form-label']));?></p>
</center>
<?php echo form_close();?>
  <?= $this->endSection('BODY') ?>