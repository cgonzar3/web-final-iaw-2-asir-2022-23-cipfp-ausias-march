<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>

Auth Admin

<?= $this->endSection('HEAD') ?>
<?= $this->section('BODY') ?>
<div style="margin: auto; margin-top: 8%;">
    <center><h1>Desactivar Usuario</h1></center>
 <center><p>¿Seguro quieres desactivar <b><?= $user->username ?></b>?</p> </center>

<?php echo form_open('auth/deactivate/' . $user->id);?>

  <center> <p>
  	<?php echo form_label(lang('Auth.deactivate_confirm_y_label'), 'confirm');?>
    <input type="radio" name="confirm" value="yes" checked="checked" />
    <?php echo form_label(lang('Auth.deactivate_confirm_n_label'), 'confirm');?>
    <input type="radio" name="confirm" value="no" />
  </p> </center>

  <?php echo form_hidden('id', $user->id); ?>

   <center><p><?php echo form_submit('submit', lang('Auth.deactivate_submit_btn'));?></p> </center>

<?php echo form_close();?>
</div>
  <?= $this->endSection('BODY') ?>