<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>

Auth Admin

<?= $this->endSection('HEAD') ?>
<?= $this->section('BODY') ?>

<br>
<center><h1 >  Crear Grupo</h1></center>
<br>


<center><div id="infoMessage"><?php echo $message;?></div></center>

<?php echo form_open("auth/create_group");?>
<center>
      <p>
            <?php echo form_label('Nombre:', 'group_name', ['class' => 'col-sm-1 col-form-label']);?> 
            <?php echo form_input($group_name);?>
      </p></center>

      <center> <p>
              
            <?php echo form_label('Descripción:', 'description', ['class' => 'col-sm-1 col-form-label']);?> 
            <?php echo form_input($description);?>
          </p></center>
<br>
      <center> <p><?php echo form_submit('submit', lang('Auth.create_group_submit_btn'));?></p></center>

<?php echo form_close();?>
  <?= $this->endSection('BODY') ?>