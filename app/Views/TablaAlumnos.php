<?php
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>

<?= $this->extend('PLANTILLAS/PlantillaHTML2') ?>

<?= $this->section('HEAD') ?>

Gestión de Alumnos
<?= $this->endSection('HEAD') ?>

<?= $this->section('BODY') ?>
<html>


    <div class="container-fluid w-50 h-100">
        <br>  <center><h1 class="text-dark" style="margin: auto;">Alumnos</h1></center>
        <div style="float:right;"><a style="max-height: 36px;" class="btn btn-dark" href="<?= site_url('nuevoalumno') ?>"> Insertar</a></div>                        
        <table class="table table-striped" id="myTable" >
            <thead>
                <tr>                  
                    <th>
                        Nombre
                    </th>
                    <th>
                        Apellidos
                    </th>
                    <th>
                        Localidad
                    </th>
                    <th>
                        Correo electrónico
                    </th>
                    <th>
                        Fecha de Nacimiento
                    </th>
                    <th>
                        Clase
                    </th>
                    <th>
                        <i>Acciones</i>
                    </th>

                </tr>
            </thead>
            <tbody>
                <?php foreach ($TodosLosAlumnos as $al): ?>
                    <tr>
                        <td>
                            <?= $al->nombre ?>
                        </td>
                        <td>
                            <?= $al->apellidos ?>
                        </td>
                        <td>
                            <?= $al->localidad ?>
                        </td> 
                        <td>
                            <?= $al->email ?>
                        </td>
                        <td>
                            <?= $al->fecha_nac ?>
                        </td> 
                         <td>
                            <?= $al->nom_clase ?>
                        </td> 
                        <td>

                            <a href="<?= base_url('/alumnos/editar/' . $al->id) ?>" title="Editar datos de <?= $al->nombre ?>">
                                <span class="bi bi-pen-fill text-dark"></span>
                            </a>                        
                            <a href="<?= base_url('/alumnos/borrar/' . $al->id) ?>" title="Borrar a <?= $al->nombre ?>
                                class="btn btn-sm btn-ligth" onclick="return confirm('¿Estás seguro de borrarlo?')" >
                                <span class="bi bi-trash-fill text-dark"></span>
                            </a>
                           
                        </td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
    
    <!--FIN TABLA-->

    <?= $this->endSection('BODY') ?>