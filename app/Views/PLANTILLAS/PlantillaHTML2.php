<?php
/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 * 
 * En esta plantilla se utilizan las siguientes secciones:
 * 
 *      - HEAD
 *      - BODY
 */


?>


<html>
    <head>
        <!--CSS PERSONALIZADO AQUÍ-->
        <style>

        @import url('https://fonts.googleapis.com/css2?family=Plus+Jakarta+Sans:wght@800&display=swap');
        *{font-family: 'Plus Jakarta Sans', sans-serif;}
        

        .CARTA{
            border: none !important;
            background: #ffffff35 !important;
            backdrop-filter: blur(15px) !important;
            border-radius: 10px !important;
        }
        
        input[type="text"]{
            background-color: #ffffffcc;
            backdrop-filter: blur(20px) !important;
            border: solid 0px black;
            border-radius: 13px;
            height: 50px;
            width: 20px;
            padding: 15px;
        }
        
        input[type="password"]{
            background-color: #ffffffcc;
            backdrop-filter: blur(20px) !important;
            border: solid 0px black;
            border-radius: 13px;
            height: 50px;
            width: 100%;
            padding: 15px;
        }
        
        body{
            background-image: url('/assets/img/bg.jpg');
            background-size: 200%;
            background-position-y: 60%;
            opacity: 100%;
            
            
        }
        
        .RadioVP{
            width: 15px;
            height: 15px;
            border: solid 1px red !important;
            margin: 5px;
            background-color: red;
            filter: grayscale(1);
            
            
        }
        
        .div_radio1 , .div_radio2 , .div_radio3{
            width: 25px;
            height: 25px;
            border-radius: 6px !important;
        }
        .div_radio1{
            display: inline-block;
            background-color: #a5c2d5;
        }
        
        .div_radio2{
            display: inline-block;
            background-color: #ff6060bb;       
        }
        
        .div_radio3{
            display: inline-block;
            background-color: #ffe072dd;         
        }        
        
        </style>
        
        
        <!-- Google Font: Source Sans Pro -->
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="assets/css/adminlte.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="assets/css/daterangepicker.css">

                    <!-- Required meta tags -->
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

                    <!-- Bootstrap CSS -->
                    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
                    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
                    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap4.css"/>
                    
                    <script type="text/javascript" src="https://code.jquery.com/jquery-3.6.0.js"></script>
                    <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.js"></script>
                    <script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap4.js"></script>
                    <script type="text/javascript">
                        $(document).ready( function () {
                            $('#myTable').DataTable();
                        } );
                    </script>   
        
        <title><?= $this->renderSection("HEAD"); ?></title>
    </head>
       <body>
         <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark navbar-dark bg-dark" style="margin: auto;font-family: 'Source Sans Pro';">
    <!-- Left navbar links -->
    <ul class="navbar-nav ">
      <li class="nav-item d-none d-sm-inline-block">
        <b><a href="/" class="nav-link" style="color: white">Verum Populi</a></b>

      </li>
      
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/alumnos" class="nav-link">Gestión de Alumnos</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/profesores" class="nav-link">Gestión de Profesorado</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/clases" class="nav-link">Gestión de Clases</a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="/auth" class="nav-link">Administrar Usuarios (IonAuth)</a>
      </li>
    </ul>

    <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
        <li class="nav-item">
        <a href="<?= base_url('/auth/logout ') ?>" style="position: relative; right: 10px">
            <button type="button" class="btn btn-outline-danger rounded-lg">Salir</button>
        </a>    
        </li>
    </ul>   
  </nav>
  <!-- /.navbar -->
         <?= $this->renderSection("BODY") ?>
        
    </body>
</html>
