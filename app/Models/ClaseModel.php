<?php

namespace App\Models;
use CodeIgniter\Model;

class ClaseModel extends Model {
    
    protected $table = 'clase';
    protected $primayKey = 'id';
    protected $returnType = 'object';    
    protected $allowedFields = ['nom_clase','id_profesor'];
    protected $validationRules = ['nom_clase' => 'required'];
    #Para opciones de validacion, mirar /Source Files/system/Language/en/Validation.php
    #Validación: 
    /*
    protected $validationRules = [
        'nombre'       =>'required|is_unique[hoteles.nombre]',
        'email'    =>'valid_email',
    ];
    */
    
}
