<?php

namespace App\Models;
use CodeIgniter\Model;

class AsistenciaModel extends Model {
    
    protected $table = 'asistencia';
    protected $primayKey = 'id';
    protected $returnType = 'object';    
    protected $allowedFields = ['id_alumno','id_profesor','fecha','estado','justificacion'];

    #Para opciones de validacion, mirar /Source Files/system/Language/en/Validation.php
    #Validación: 
    /*
    protected $validationRules = [
        'nombre'       =>'required|is_unique[hoteles.nombre]',
        'email'    =>'valid_email',
    ];
    */
    
}
