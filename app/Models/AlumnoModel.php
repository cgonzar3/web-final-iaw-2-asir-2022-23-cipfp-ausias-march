<?php

namespace App\Models;
use CodeIgniter\Model;

class AlumnoModel extends Model {
    
    protected $table = 'alumno';
    protected $primayKey = 'id';
    protected $returnType = 'object';    
    protected $allowedFields = ['nombre','apellidos','localidad','email','fecha_nac','id_profesor','id_clase'];
    protected $validationRules = [
        'nombre'    => 'required',
        'apellidos' => 'required',
        'localidad' => 'required',
        'email'     => 'required',
        'fecha_nac' => 'required',
        
    ];
    #Para opciones de validacion, mirar /Source Files/system/Language/en/Validation.php
    #Validación: 
    /*
    protected $validationRules = [
        'nombre'       =>'required|is_unique[hoteles.nombre]',
        'email'    =>'valid_email',
    ];
    */
    
}
