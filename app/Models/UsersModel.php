<?php

namespace App\Models;
use CodeIgniter\Model;

class UsersModel extends Model {
    
    protected $table = 'users';
    protected $primayKey = 'id';
    protected $returnType = 'object';    
    protected $allowedFields = ['first_name'];
    protected $validationRules = [
       #rules
    ];
    #Para opciones de validacion, mirar /Source Files/system/Language/en/Validation.php
    #Validaci  n: 
    /*
    protected $validationRules = [
        'nombre'       =>'required|is_unique[hoteles.nombre]',
        'email'    =>'valid_email',
    ];
    */
    
}