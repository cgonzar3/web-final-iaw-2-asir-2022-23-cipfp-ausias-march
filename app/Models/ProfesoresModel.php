<?php

namespace App\Models;
use CodeIgniter\Model;

class ProfesoresModel extends Model {
    
    protected $table = 'profesor';
    protected $primayKey = 'id';
    protected $returnType = 'object';    
    protected $allowedFields = ['usuario','nombre','apellidos'];
    protected $validationRules = [
        'usuario' => 'required',
        'nombre'     => 'required',
        'apellidos'     => 'required',
    ];
    #Para opciones de validacion, mirar /Source Files/system/Language/en/Validation.php
    #Validación: 
    /*
    protected $validationRules = [
        'nombre'       =>'required|is_unique[hoteles.nombre]',
        'email'    =>'valid_email',
    ];
    */
    
}
