<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;

use CodeIgniter\HTTP\CLIRequest;
use CodeIgniter\HTTP\IncomingRequest;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Psr\Log\LoggerInterface;
use CodeIgniter\Controller;
use App\Models\PostModel;
use App\Models\CommentModel;
/**
 * Description of Profe
 *
 * @author carlos
 */
class Profe extends BaseController {
    
    protected $post;
    
    public function initController(RequestInterface $request, ResponseInterface $response, LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        // Preload any models, libraries, etc, here.

        $this->post = new \App\Models\AlumnoModel();
        $this->post = new \App\Models\AsistenciaModel();
        $this->post = new \App\Models\ClaseModel();
        $this->post = new \App\Models\ProfesoresModel();
    }
    
    public function inicio(){
        
        $array['ionAuth'] = $this->ionAuth;
        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            return redirect()->to('/login');
        }
        
        $u = $this->ionAuth->user()->row();
        $array['usuario']= $u->first_name;
        $array['USERNAME_ID']= $u->id; 
        
       if ($u->username == "administrator") {
           return redirect()->to( base_url()."/admin");
           
       }
        
        $ClaseModel = new \App\Models\ClaseModel;
        $ProfeModel = new \App\Models\ProfesoresModel;
               

        $array['SelectClases']= $ClaseModel->like(['id_profesor' => $array['USERNAME_ID']])->findAll();
        

        
                    
        return view('UsersApp/UserInicio',$array);
        
    }
    
    public function clase($ID_CLASE){
        
        $array['ionAuth'] = $this->ionAuth;
        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            return redirect()->to('/login');
        }
        
        
        $ID_PROFE_LOGGEADO = 1;
        $ClaseModel = new \App\Models\ClaseModel;
        $u = $this->ionAuth->user()->row();
        $array['usuario']= $u->first_name ;
        $array['CLASE']= "$ID_CLASE";
        #Extrae el nombre de la clase según la ID en la URL.
        $array['NombreClase']= $ClaseModel->like(['id' => $ID_CLASE])->findAll();
        # Para hacer referencia en la vista: $NombreClase['0']->nom_clase
        
        
        $array['SelectClases']= $ClaseModel->like(['id' => $ID_CLASE])->findAll();
        
        #echo "<pre>";
        #print_r($array);
        #echo "</pre>";        
        return view('UsersApp/Clase',$array);
    }
    
    public function PasarLista($ID_CLASE){
        
        $array['ionAuth'] = $this->ionAuth;
        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            return redirect()->to('/login');
        }
        
        helper('form');
        $ID_PROFE_LOGGEADO = 1;
       
        $ClaseModel = new \App\Models\ClaseModel;
        $AlumnoModel = new \App\Models\AlumnoModel();
        $AsistenciaModel = new \App\Models\AsistenciaModel();
        
        $array['ExisteAsistenciaHoy'] = $AsistenciaModel->like(['fecha' => date('l jS F Y')])->findAll();
        
        
        $u = $this->ionAuth->user()->row();
        $array['usuario']= $u->first_name;
        $array['CLASE']= "$ID_CLASE";
        //$array['SelectAlumnos']= $AlumnoModel->like(['id_clase' => $ID_CLASE])->findAll();
        $array['SelectAlumnos']= $AlumnoModel->select('alumno.id,alumno.nombre,alumno.apellidos,alumno.id_clase')
                                            ->like(['id_clase'=>$ID_CLASE])
                                            ->findAll();
        #Extrae el nombre de la clase según la ID en la URL.
        $array['NombreClase']= $ClaseModel->like(['id' => $ID_CLASE])->find();
        # Para hacer referencia en la vista: $NombreClase['0']->nom_clase
        #echo "<pre>";
        #print_r($array);
        #echo "</pre>";
        return view('UsersApp/PasarLista',$array);
    }
    

    
    public function PasarListaSubmitInsert($ID_CLASE){
        
        $array['ionAuth'] = $this->ionAuth;
        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            return redirect()->to('/login');
        }
        
        $AsistenciaModel = new \App\Models\AsistenciaModel();
        $alumnos_asistencia = $this->request->getPost();
        unset($alumnos_asistencia['myTable_length']);
        unset($alumnos_asistencia['boton_submit']);
       
        foreach($alumnos_asistencia as $al) {
            $ESTADO = explode("-", $al);
            $array = [
                'id_alumno' => $ESTADO['0'],
                'fecha'=>date('l jS F Y'),
                'estado'=> $ESTADO['1'],
            ];
 
            $AsistenciaModel->insert($array);
        }           
        
                    helper('form');
                    $ID_PROFE_LOGGEADO = 1;
                    $ClaseModel = new \App\Models\ClaseModel;
                    $AlumnoModel = new \App\Models\AlumnoModel();
                   

                    $array['ExisteAsistenciaHoy'] = $AsistenciaModel->like(['fecha' => date('l jS F Y')])->findAll();


                    $u = $this->ionAuth->user()->row();
                    $array['usuario']= $u->first_name;
                    $array['CLASE']= "$ID_CLASE";
                    //$array['SelectAlumnos']= $AlumnoModel->like(['id_clase' => $ID_CLASE])->findAll();
                    $array['SelectAlumnos']= $AlumnoModel->select('alumno.id,alumno.nombre,alumno.apellidos,alumno.id_clase')
                                                        ->like(['id_clase'=>$ID_CLASE])
                                                        ->findAll();
                    #Extrae el nombre de la clase según la ID en la URL.
                    $array['NombreClase']= $ClaseModel->like(['id' => $ID_CLASE])->find();
                    # Para hacer referencia en la vista: $NombreClase['0']->nom_clase
      
        return view('UsersApp/PasarLista',$array);
        
    }
    
    public function DatosAlumnos($ID_CLASE){
        
        $array['ionAuth'] = $this->ionAuth;
        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            return redirect()->to('/login');
        }
        
        $ClaseModel = new \App\Models\ClaseModel;
        $AlumnoModel = new \App\Models\AlumnoModel();
        //$AsistenciaModel = new \App\Models\AsistenciaModel();
        
        $array['CLASE']= "$ID_CLASE";
        
        #Extrae el nombre de la clase según la ID en la URL.
        $array['NombreClase']= $ClaseModel->like(['id' => $ID_CLASE])->findAll();
        # Para hacer referencia en la vista: $NombreClase['0']->nom_clase
        
        $array['SelectAlumnos']= $AlumnoModel->like(['id_clase' => $ID_CLASE])->findAll();
        
        return view('UsersApp/DatosAlumnos',$array);
        
    }
    
    public function AsistenciasAlumnos($ID_CLASE){
        
        $array['ionAuth'] = $this->ionAuth;
        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            return redirect()->to('/login');
        }
        
        $ClaseModel = new \App\Models\ClaseModel;
        $AlumnoModel = new \App\Models\AlumnoModel();
        //$AsistenciaModel = new \App\Models\AsistenciaModel();
        
        $array['CLASE']= "$ID_CLASE";
        
        #Extrae el nombre de la clase según la ID en la URL.
        $array['NombreClase']= $ClaseModel->like(['id' => $ID_CLASE])->findAll();
        # Para hacer referencia en la vista: $NombreClase['0']->nom_clase
        
        $array['SelectAlumnos']= $AlumnoModel->like(['id_clase' => $ID_CLASE])->findAll();
        
        return view('UsersApp/AsistenciasAlumnos',$array);
        
    }
    
    public function MuestraAsistencias($ID_ALUMNO){
        
        $array['ionAuth'] = $this->ionAuth;
        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            return redirect()->to('/login');
        }
        
        //$ClaseModel = new \App\Models\ClaseModel;
        $AlumnoModel = new \App\Models\AlumnoModel();
        $AsistenciaModel = new \App\Models\AsistenciaModel();
        
        $array['ALUMNO']= "$ID_ALUMNO";
        
        #Extrae el nombre del alumno según la ID en la URL.
        $array['NombreAlumno']= $AlumnoModel->like(['id' => $ID_ALUMNO])->findAll();
        # Para hacer referencia en la vista: NombreAlumno['0']->nombre
        
        $array['SelectAsistencias']= $AsistenciaModel->like(['id_alumno' => $ID_ALUMNO])->findAll();
      
        return view('UsersApp/MuestraAsistencias',$array);
        
    }
    
    public function JustificaAsistencias($ID_ALUMNO,$ID_ASIST_JUSTIFICADA){
        
        $array['ionAuth'] = $this->ionAuth;
        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            return redirect()->to('/login');
        }
        
        //$ClaseModel = new \App\Models\ClaseModel;
        $AlumnoModel = new \App\Models\AlumnoModel();
        $AsistenciaModel = new \App\Models\AsistenciaModel();
        $array['ALUMNO']= "$ID_ALUMNO";
        
        #Extrae el nombre del alumno según la ID en la URL.
        $array['NombreAlumno']= $AlumnoModel->like(['id' => $ID_ALUMNO])->findAll();
        # Para hacer referencia en la vista: NombreAlumno['0']->nombre
        
        # Se justifica la falta seleccionada:
                $AsistenciaModel->set(['estado' => 'falta_j'])
                        ->where('id', $ID_ASIST_JUSTIFICADA)
                        ->update();
        
        $array['SelectAsistencias']= $AsistenciaModel->like(['id_alumno' => $ID_ALUMNO])->findAll();
      

        
        return view('UsersApp/MuestraAsistencias',$array);
        
    }    
    
}
