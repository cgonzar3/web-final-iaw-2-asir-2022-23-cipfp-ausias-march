<?php

namespace App\Controllers;

class Aplicacion extends BaseController
{
    public function alumnos(){
        
        $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        

        
        $AlumnoModel = new \App\Models\AlumnoModel();
        $claseModel = new \App\Models\ClaseModel();
        $ResultadoSelect['TodosLosAlumnos'] = $AlumnoModel->select('alumno.id,alumno.nombre,alumno.apellidos,alumno.localidad,alumno.email,alumno.fecha_nac,clase.nom_clase')
                                                          ->join('clase','clase.id=alumno.id_clase','LEFT')
                                                          ->findAll();
             
        return view("TablaAlumnos",$ResultadoSelect);
        
        #echo "<pre>";
        #print_r($ResultadoSelect);
        #echo "</pre>";
        
        
    }
     public function  alumnosEditar($id){
         
         $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        

         
         
         $claseModel = new \App\Models\ClaseModel();
         $data['TodasLasClases'] =$claseModel->findAll();
         helper('form');
        $AlumnoModel = new \App\Models\AlumnoModel();
        $data['al'] = $AlumnoModel->find($id);
        if (strtolower($this->request->getMethod()) !== 'post') { 
            //desde un get botón o url
            return view('EditarAlumno',$data);
        } else {
            $al = $this->request->getPost();
            unset($al['boton']);
            /*echo '<pre>';
              print_r($alumno);
              echo '</pre>';*/
            $AlumnoModel ->setValidationRule('nombre', 'required|min_length[3]');
            if ($AlumnoModel ->update($id, $al) === false){
                //hay un error al actualizar
                $data['errores'] = $AlumnoModel ->errors();
                return view('EditarAlumno',$data);
            }
        }        
        return redirect()->to('alumnos');
    }
    
    public function insertAlumnos(){
        $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        

        $claseModel = new \App\Models\ClaseModel();
        $data['TodasLasClases'] = $claseModel->findAll();

        helper('form');
        if (strtolower($this->request->getMethod()) !== 'post') {
            return view('InsertAlumno', $data);
        } else {
            $alumnoNuevo = [
                'nombre' => $this->request->getPost('nombre'),
                'apellidos' => $this->request->getPost('apellidos'),
                'localidad' => $this->request->getPost('localidad'),
                'email' => $this->request->getPost('email'),
                'fecha_nac' => $this->request->getPost('fecha_nac'),
                'id_clase' => $this->request->getPost('id_clase'),
            ];
            
            echo "<pre>";
            print_r($alumnoNuevo);
            echo "</pre>";
            
            $alumnoModel = new \App\Models\AlumnoModel();
            if ($alumnoModel->insert($alumnoNuevo) === false) {
                $data['errores'] = $alumnoModel->errors();
                return view('InsertAlumno', $data);
            }
        }
        return redirect()->to('alumnos');
    }
     public function delete($id){
         $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        
 
         
         
      $alumnoModel = new \App\Models\AlumnoModel();
       $alumnoModel->delete(['id'=>$id]);
        return redirect()->to('alumnos');
        
     }
     public function admin(){
         
       $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        


         
         
       $profesModel = new \App\Models\ProfesoresModel();
       $alumnoModel = new \App\Models\AlumnoModel(); 
       $claseModel = new \App\Models\ClaseModel();
       $usersModel = new \App\Models\UsersModel();
       $data ['clases'] = $claseModel->select('COUNT(nom_clase) as a')->findAll();
       $data ['alumnos'] = $alumnoModel->select('COUNT(nombre) as a')->findAll();  
       $data ['profes'] = $profesModel->select('COUNT(id) as a')->findAll();
       $data ['users'] = $usersModel->select('COUNT(id) as a')->findAll();
        return view('indice',$data); 
         
     }
     //-----------------------------------------------------------------------------
     //PROFESORESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
     
     public function profesores(){
        $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        
 
         
         
        $profesModel = new \App\Models\ProfesoresModel();
        $ResultadoSelect['TodosLosProfesores'] = $profesModel->findAll();
        
        return view("profesores/TablaProfesores",$ResultadoSelect);
        
        #echo "<pre>";
        #print_r($ResultadoSelect);
        #echo "</pre>";
     }
     public function insertProfesores(){
         $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        

         
        $data['titulo'] = 'Nuevos Prodesores';
        helper('form');
       if (strtolower($this->request->getMethod()) !== 'post') {
           return view('profesores/InsertProfesor', $data); 
        } else {
            $profesorNuevo = [
            'usuario' => $this->request->getPost('usuario'),
            'nombre' => $this->request->getPost('nombre'),
            'apellidos' => $this->request->getPost('apellidos'),
        ];
           $profesorModel = new \App\Models\ProfesoresModel();
            if ($profesorModel->insert($profesorNuevo) === false){
               $data['errores'] = $profesorModel->errors(); 
               return view('profesores/InsertProfesor', $data);  
            }
            
        }
         return redirect()->to('profesores');
    }
    
    public function  profesoresEditar($id){
        $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        

        helper('form');
        $profesorModel = new \App\Models\ProfesoresModel();
        $data['prof'] = $profesorModel->find($id);
        if (strtolower($this->request->getMethod()) !== 'post') { 
            //desde un get botón o url
            return view('profesores/EditarProfesor',$data);
        } else {
            $prof = $this->request->getPost();
            unset($prof['boton']);
            /*echo '<pre>';
              print_r($alumno);
              echo '</pre>';*/
            $profesorModel ->setValidationRule('nombre', 'required|min_length[3]');
            if ($profesorModel ->update($id, $prof) === false){
                //hay un error al actualizar
                $data['errores'] = $profesorModel ->errors();
                return view('profesores/EditarProfesor',$data);
            }
        }        
        return redirect()->to('profesores');
    }
     public function deleteprofesor($id){
         $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        

      $profesorModel = new \App\Models\ProfesoresModel();
       $profesorModel->delete(['id'=>$id]);
        return redirect()->to('profesores');
        
     }
     //-----------------------------------------------------------------------------
     //-----------------------------------------------------------------------------
     //CLASEEEEEEEEEEESSSSSSSSSSSSSSSSSSSSSSSSSSS
     //-----------------------------------------------------------------------------
     //-----------------------------------------------------------------------------
     
         public function clases(){
             $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        

        $claseModel = new \App\Models\ClaseModel();
        $profesorModel = new \App\Models\ProfesoresModel();
        $ResultadoSelect['TodasLasClases'] = $claseModel->select('clase.id,clase.nom_clase,profesor.nombre,profesor.apellidos')
                                                          ->join('profesor','profesor.id=clase.id_profesor','LEFT')
                                                          ->findAll();
        return view("clases/TablaClases",$ResultadoSelect);
       #  $ResultadoSelect['TodosLosAlumnos'] = $AlumnoModel->select('alumno.id,alumno.nombre,alumno.apellidos,alumno.localidad,alumno.email,alumno.fecha_nac,clase.nom_clase')
        #                                                  ->join('clase','clase.id=alumno.id_clase','LEFT')
         #                                                 ->findAll();
                echo '<pre>';
              print_r($ResultadoSelect);
              echo '</pre>';
         
         }  
         public function insertClase(){
             $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        

         $profesModel = new \App\Models\ProfesoresModel();
        $data['TodosLosProfesores'] = $profesModel->findAll();
        $data['titulo'] = 'Nuevas Clases';
        helper('form');
       if (strtolower($this->request->getMethod()) !== 'post') {
           return view('clases/InsertClase', $data); 
        } else {
            $clasesNuevo = [
            'nom_clase' => $this->request->getPost('nom_clase'),
            'id_profesor' => $this->request->getPost('id_profesor'),
        ];
           $claseModel = new \App\Models\ClaseModel();
            if ($claseModel->insert($clasesNuevo) === false){
               $data['errores'] = $claseModel->errors(); 
               return view('clases/InsertClase', $data);  
            }
            
        }
         return redirect()->to('clases');
    }
         
       public function  clasesEditar($id){
           $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        

           $profesModel = new \App\Models\ProfesoresModel();
        $data['TodosLosProfesores'] = $profesModel->findAll();
        helper('form');
        $claseModel = new \App\Models\ClaseModel();
        $data['clas'] = $claseModel->find($id);
        if (strtolower($this->request->getMethod()) !== 'post') { 
            //desde un get botón o url
            return view('clases/EditarClase',$data);
        } else {
            $clas = $this->request->getPost();
            unset($clas['boton']);
            $claseModel ->setValidationRule('nom_clase', 'required|min_length[3]');
            if ($claseModel ->update($id, $clas) === false){
                //hay un error al actualizar
                $data['errores'] = $claseModel ->errors();
                return view('clases/EditarClase',$data);
            }
        }        
        return redirect()->to('clases');
    }   
    public function deleteclases($id){
        $array['ionAuth'] = $this->ionAuth;
       $u = $this->ionAuth->user()->row();

        // Si no se ha iniciado sesión, devolver a página de LOGIN.
        if ( ! $array['ionAuth']->loggedIn() ){
            $array['iniciado'] = 'iniciado';
            return redirect()->to('/login');
        }   
        
        if (  $u->username != "administrator" ) {
           return redirect()->to( base_url()."/");
        }        

      $claseModel = new \App\Models\ClaseModel();
       $claseModel->delete(['id'=>$id]);
        return redirect()->to('clases');
        
     }
    
     
   
     
     
     
     
}
