<?php namespace Config;

class IonAuth extends \IonAuth\Config\IonAuth
{
    // set your specific config
    public $siteTitle    = 'VerumPopuli';
    public $adminEmail   = 'nomail@verumpopuli.com';
    // public $emailTemplates           = 'App\\Views\\auth\\email\\';
    // ...
}
