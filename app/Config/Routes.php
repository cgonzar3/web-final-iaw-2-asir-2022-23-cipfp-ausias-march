<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */



//////////////// RUTAS IONAUTH ///////////////////////

	$routes->add('/auth/login', 'Auth::login');
	$routes->get('/auth/logout', 'Auth::logout');
	$routes->add('/auth/forgot_password', 'Auth::forgot_password');
	$routes->get('/auth', 'Auth::index');
	$routes->add('/auth/create_user', 'Auth::create_user');
	$routes->add('/auth/edit_user/(:num)', 'Auth::edit_user/$1');
	$routes->add('/auth/create_group', 'Auth::create_group');
	$routes->get('/auth/activate/(:num)', 'Auth::activate/$1');
	$routes->get('/auth/activate/(:num)/(:hash)', 'Auth::activate/$1/$2');
	$routes->add('/auth/deactivate/(:num)', 'Auth::deactivate/$1');
	$routes->get('/auth/reset_password/(:hash)', 'Auth::reset_password/$1');
	$routes->post('/auth/reset_password/(:hash)', 'Auth::reset_password/$1');

    $routes->add('/login', 'Auth::login');
    

// Sección carlos

$routes->get('/a', 'Profe::inicio');
$routes->get('/username/inicio', 'Profe::inicio');
$routes->get('/username/clase/(:alphanum)', 'Profe::clase/$1');
$routes->get('/username/pasar_lista/(:alphanum)', 'Profe::PasarLista/$1');
$routes->get('/username/datos-alumnos/(:alphanum)', 'Profe::DatosAlumnos/$1');
$routes->get('/username/asistencias-alumnos/(:alphanum)', 'Profe::AsistenciasAlumnos/$1');
$routes->get('/username/asistencias-de/(:num)', 'Profe::MuestraAsistencias/$1');
$routes->get('/username/asistencias-de/justif/(:num)/(:num)', 'Profe::JustificaAsistencias/$1/$2');


$routes->post('/username/pasar_lista/(:alphanum)', 'Profe::PasarListaSubmitInsert/$1');

$routes->get('/cerrarsesion','Login::cerrar_sesion');


////////////////////////////////////


// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Profe::inicio');
$routes->get('/admin', 'Aplicacion::admin');

//LISTA DE ALUMNOS
$routes->get('/alumnos', 'Aplicacion::alumnos');

//INSERTAR
$routes->get('/nuevoalumno', 'Aplicacion::insertAlumnos');
$routes->post('/nuevoalumno', 'Aplicacion::insertAlumnos');

//EDITAR
$routes->get('/alumnos/editar/(:num)', 'Aplicacion::alumnosEditar/$1');
$routes->post('/alumnos/editar/(:num)', 'Aplicacion::alumnosEditar/$1');

//BORRAR
$routes->get('/alumnos/borrar/(:num)', 'Aplicacion::delete/$1');
$routes->post('/alumnos/borrar/(:num)', 'Aplicacion::delete/$1');

//PROFESORESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
//------------------------------------------------------------------
//
//LISTA DE ALUMNOS
$routes->get('/profesores', 'Aplicacion::profesores');

//INSERTAR
$routes->get('/nuevoprofesor', 'Aplicacion::insertProfesores');
$routes->post('/nuevoprofesor', 'Aplicacion::insertProfesores');

//EDITAR
$routes->get('/profesores/editar/(:num)', 'Aplicacion::profesoresEditar/$1');
$routes->post('/profesores/editar/(:num)', 'Aplicacion::profesoresEditar/$1');

//BORRAR
$routes->get('/profesores/borrar/(:num)', 'Aplicacion::deleteprofesor/$1');
$routes->post('/profesores/borrar/(:num)', 'Aplicacion::deleteprofesor/$1');

//CLASESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

$routes->get('/clases', 'Aplicacion::clases');

//INSERTAR
$routes->get('/nuevaclase', 'Aplicacion::insertClase');
$routes->post('/nuevaclase', 'Aplicacion::insertClase');

//EDITAR
$routes->get('/clases/editar/(:num)', 'Aplicacion::clasesEditar/$1');
$routes->post('/clases/editar/(:num)', 'Aplicacion::clasesEditar/$1');

//BORRAR
$routes->get('/clases/borrar/(:num)', 'Aplicacion::deleteclases/$1');
$routes->post('/clases/borrar/(:num)', 'Aplicacion::deleteclases/$1');


//
// $routes->add('auth/login', 'Auth::login');
// $routes->add('auth', 'Auth::index');
// $routes->add('auth/create_user', 'Auth::create_user');
// $routes->add('auth/create_group', 'Auth::create_group');
// $routes->add('auth/deactivate/(:num)', 'Auth::deactivate/$1');
// $routes->add('auth/activate/(:num)', 'Auth::activate/$1');
// $routes->add('auth/edit_user/(:num)', 'Auth::edit_user/$1');
 $routes->add('auth/edit_group/(:num)', 'Auth::edit_group/$1');
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

